# fundamento-de-arquitetura-de-sistemas-dio

Author: [Robinson Dias](https://github.com/robinson-1985)

## Definição

- Olá, tudo bem? Sejam bem vindos ao curso de Fundamentos de Arquitetura de Sistemas da Digital Innovation One.
- Nesse curso aprendemos como funcionam as arquiteturas de softwares, seus conceitos, aplicações e desenvolvimento de operações.
- Esse módulo do curso está presente no link: [Fundamentos de Arquitetura de Sistemas](https://web.digitalinnovation.one/course/fundamentos-da-arquitetura-de-sistemas/learning/bd1cf015-6d40-4e38-a409-636ba6a9cb98?back=/track/cognizant-cloud-data-engineer).

# CONCEITO DE ARQUITETURA EM APLICAÇÕES PARA INTERNET. 

## Tipos de arquiteturas

### Monolito

![Monolito](https://raw.githubusercontent.com/jeffhsta/fundamentos_arquitetura/master/monolito.png)

### Microserviços #1

![Monolito](https://raw.githubusercontent.com/jeffhsta/fundamentos_arquitetura/master/microservicos1.png)

### Microserviços #2

![Monolito](https://raw.githubusercontent.com/jeffhsta/fundamentos_arquitetura/master/microservicos2.png)

### Microserviços #3

![Monolito](https://raw.githubusercontent.com/jeffhsta/fundamentos_arquitetura/master/microservicos3.png)
