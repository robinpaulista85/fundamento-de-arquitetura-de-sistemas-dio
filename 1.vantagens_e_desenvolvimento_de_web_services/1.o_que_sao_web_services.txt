- Serviços Web ou Web Services são soluções para aplicações se comunicarem independente de linguagem, software e hardwares utilizados;

- Inicialmente Serviços Web foram criados para trocas de mensagens utilizando a linguagem XML (Extensible markup language) sobre o protocolo HTTP sendo identificado por URI (Uniform Resource Identifier).

- Podemos dizer que Serviços Web são API's que se comunicam por meio de redes sobre o protocolo HTTP.

- XML e JSON são linguagens de maracações (markup).

      Exemplo de código em XML                   

<endereco>
    <cep>9999-999</cep>
    <bairro>Jardim Paulista</bairro>
    <logradouro>Avenida Paulista</logradouro>
    <cidade>São Paulo</cidade>
    <numero>99</numero>
</endereco>

    Exemplo de código em JSON                  

{
    "endereco": {
        "cep": ""9999-9999",
        "bairro": "Jardim Paulista",
        "logradouro": "Avenida Paulista",
        "cidade": "São Paulo",
        "numero": 99
    }
}

                            Vantagens

- Linguagem comum;
- Integração mais faciliatadas;
- Reutilização de implementação;
- Segurança melhor;
- Custos mais baixos por conta da integração de dados. 

                            Principais tecnologias

- SOAP, REST, XML, JSON.